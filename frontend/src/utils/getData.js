import axios from 'axios';
import { trackPromise } from 'react-promise-tracker';
import { server } from "../config"

export async function getData(formData) {
    const config = { headers : {
        'Content-type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    }};
    try{
        const data = await trackPromise(
            axios.post( server + '/', formData, config))
        return data
    }catch (err) {
        return err.response;
    }
};


export async function changeData(name, transcript) {
    const config = { headers : {
        'Access-Control-Allow-Origin': '*',
    }};
    try{
        const data = await 
            axios.get(server + '/get', {
                params:{
                    transcript:  transcript, 
                    name: name
                }
            }, config)
        return data
    }catch (err) {
        return err.response;
    }
};

export async function showGoogle(name) {
    const config = { headers : {
        'Access-Control-Allow-Origin': '*',
    }};
    try{
        const data = await 
            axios.get(server + '/get_trans', {
                params:{
                    name: name
                }
            }, config)
        return data
    }catch (err) {
        return err.response;
    }
};

export async function saveData() {
    const config = { headers : {
        'Access-Control-Allow-Origin': '*',
    }};
    try{
        const data = await 
            axios.get(server + '/save', config)
        return data
    }catch (err) {
        return err.response;
    }
};