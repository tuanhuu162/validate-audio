import React from "react";
import { Audio } from "./Audio"
import { ListGroup, Media, Card, Alert } from "react-bootstrap";
import { AudioPlayer } from "./AudioPlayer"
import LazyLoad from 'react-lazyload'

const Loading = () => (
  <div className="loading"  style={{ display:"flex", justifyContent:"center", alignItem:"center", witdh:"100%" }}>
        <Alert variant='warning'>
            Loading...............................!!!!!!!!!!!!!!!!!!
        </Alert>
  </div>
)

export function ListAudio(props){
    return (
        <div>
            <ListGroup>
                { props.data && props.data.map((item, i) => (
                    <LazyLoad
                    key={i}
                    height={100}
                    offset={[-100, 100]}
                    placeholder={<Loading />}
                  >
                    <ListGroup.Item key={i}>
                           <Card>
                                <Media>
                                    <AudioPlayer name={item['name']}/>
                                    <Media.Body className="p-1 mx-auto">
                                        <Audio audio={item}/>
                                    </Media.Body>
                                </Media>
                            </Card>
                    </ListGroup.Item>
                  </LazyLoad>
                ))}
            </ListGroup>
        </div>
    );
};