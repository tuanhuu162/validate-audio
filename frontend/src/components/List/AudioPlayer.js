import React from "react";
import { useAudio } from "../../hooks/useAudio";
import { server } from "../../config"

export function AudioPlayer(props){
    const [ playing, toggle ] = useAudio( server + "/get?name=" + props.name)

    return (
        <img
            width={100}
            height={100}
            className="p-1 mx-auto m-auto "
            src={playing ? "/pause.png":"/play.png"}
            alt="Volume"
            padding={20}
            onClick={toggle}        
        />
    );
};