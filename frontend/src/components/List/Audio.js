import React, { useState } from "react";
import "./Audio.css";
import { Button, InputGroup, FormControl } from "react-bootstrap";
import { changeData, showGoogle } from "../../utils/getData" ;

export function Audio(props){
    const [ tmp, setTmp ] = useState(props.audio.text);
    const [ change, setChange ] = useState(false);
    const [ style, setStyle ] = useState(props.audio.status === "#f6ff00" ?{
        backgroundColor: "#f7ff05",
        color: "black"
    }:{color: props.audio.status});
    const [ compare, setCompare ] = useState(props.audio.google !== "" ? true:false);
    const [ google, setGoogle ] = useState(props.audio.google);

    const handleChange = (event) => {
        let new_text = event.target.value;
        setTmp(new_text);
        setChange(true);
    }

    const handleGoogle = (event) => {
        showGoogle(props.audio.name)
        .then(response => {
            setCompare(true);
            setGoogle(response.data.google)
            setStyle({
                color: response.data.status
            });            
            console.log(response.data.status);
        })
        .catch(function (error) {
            console.log(error);
            alert(error);
        });
    }

    const handleSubmit = (event) => {
        changeData(props.audio.name, tmp)
            .then(response => {
                console.log(response.data.message);
                setChange(false);
                setStyle({
                    backgroundColor: "#f7ff05",
                    color: "black"
                });
            })
            .catch(function (error) {
                console.log(error);
                alert(error);
            });
    }


    return (
        <div>    
            <InputGroup className="p-1 m-auto">
                <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default">Transcript</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    as="textarea"
                    className="transcript"
                    aria-label="Default"
                    aria-describedby="inputGroup-sizing-default"
                    value={tmp}
                    style={style}
                    onChange={handleChange}
                />
            </InputGroup>
            { !compare?  
            <Button variant="primary" type="button" onClick={handleGoogle} className="mr-1">
                Show google transcript
            </Button>: 
            <InputGroup className="p-1 m-auto">
                <InputGroup.Prepend>
                <InputGroup.Text id="inputGroup-sizing-default">Google result</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl 
                    as="textarea"
                    className="transcript"
                    aria-label="Default"
                    aria-describedby="inputGroup-sizing-default"
                    value={google}
                    style={style}
                    readOnly
                />
            </InputGroup>}
            { change? <Button variant="primary" type="button" onClick={handleSubmit}>
                        Save
                    </Button> : null }
        </div> 
    );
};