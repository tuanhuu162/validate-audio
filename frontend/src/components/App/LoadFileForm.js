import React, { useState, useEffect } from "react";
import { Form, Button, Row, Col, Alert } from "react-bootstrap";
import bsCustomFileInput from 'bs-custom-file-input';
import { useInput } from "../../hooks/useInput";
import { getData, saveData } from "../../utils/getData";
import { ListAudio } from "../List/ListAudio";
import { server } from "../../config"


export function LoadFileForm(props) {
    const { value:audio, bind:bindAudio, reset:resetAudio } = useInput('');
    const { value:transcript, bind:bindTranscript, reset:resetTranscript } = useInput('');
    const [ data, setData ] = useState([]);
    
    useEffect(() => {
        getData().then(response => {
            if(response.data != null){
                console.log(response.data.data);
                setData(response.data.data);
            } else {
                alert("Your wav data does not match your transcription!!!!!!!!!")
            }
        })
        .catch(function (error) {
            console.log(error);
            alert("File cannot be handled!!!!!!!");
        });
    }, [])

    function handleClick(event) {
        saveData().then(response => {
            console.log(response.data.message);
            alert('Save successfully!!!!!!!!!');
        })
        .catch(function (error) {
            console.log(error);
            alert(error);
        });
    }

    return (
        <div>
        {data.length === 0 ? <Alert variant='info'>
            Loading...............................!!!!!!!!!!!!!!!!!!
        </Alert>:<div style={{ display:"flex", justifyContent:"center", alignItem:"center" }}>
            <audio src="/thisissparta.swf.mp3" autoPlay></audio>
            <Button variant="primary" type="button" onClick={handleClick} className="ml-5 p-3">
                Save all transcripts
            </Button>
        </div>}
        <ListAudio data={data}/>
        </div>
    );    
};

    // const handleSubmit = (evt) => {
    //     evt.preventDefault();
    //     if (audio === '' || transcript === '') {
    //         alert("Fill all params!!!!!!!");

    //     } else {
    //         console.log(`Submitting Name ${audio.name} ${transcript.name}`);
    //         const formData = new FormData();
    //         formData.append("audio", audio, audio.name);
    //         formData.append("transcription", transcript, transcript.name);
    //         const res = getData(formData);
    //         res.then(response => {
    //             if(response.data != null){
    //                 console.log(response.data.data);
    //                 setData(response.data.data);
    //             } else {
    //                 alert("Your wav data does not match your transcription!!!!!!!!!")
    //             }
    //             resetAudio();
    //             resetTranscript();
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //             alert("File cannot be handled!!!!!!!");
    //         });
    //     }
    // }

    // useEffect(() => bsCustomFileInput.init());

//     return (
//         <div>
//         {data.length === 0 ? <Form onSubmit={handleSubmit}>
//             <Row>
//                 <Col border="primary" className="ml-4">
//                     <div className="custom-file">
//                         <Form.Group>
//                             <Form.File.Input id="audioFile" name="audioFile" className="custom-file-input" onChange={bindAudio.onChange}/>
//                             <Form.File.Label className="custom-file-label" htmlFor="audioFile" >Choose audio zip file</Form.File.Label>
//                         </Form.Group>
//                     </div>
//                 </Col>
//                 <Col border="primary">
//                     <div className="custom-file">
//                         <Form.Group>
//                             <Form.File.Input id="transcript" type="file" name="transcript" className="custom-file-input" onChange={bindTranscript.onChange}/>
//                             <Form.File.Label className="custom-file-label" htmlFor="transcript" >Choose transcription csv file</Form.File.Label>
//                         </Form.Group>
//                     </div>
//                 </Col>
//             </Row>
//             <br/>
//             <div style={{ display:"flex", justifyContent:"center", alignItem:"center" }}>
//                 <Button variant="primary" type="submit" className=" p-3">
//                     Submit
//                 </Button>
//             </div>
//         </Form>:<div style={{ display:"flex", justifyContent:"center", alignItem:"center" }}>
//             <audio src="/thisissparta.swf.mp3" autoPlay></audio>
//             <Button variant="primary" type="button" href="http://localhost:5000/save" className="ml-5 p-3">
//                 Save all transcripts
//             </Button>
//         </div>}
//         <ListAudio data={data}/>
//         </div>
//     );    
// };