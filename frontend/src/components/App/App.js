import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { LoadFileForm } from "./LoadFileForm";
import { usePromiseTracker } from 'react-promise-tracker';
import Loader from 'react-promise-loader';

function App() {

  return (
    <div className="App">
        <div className="App-header" style={{ display:"flex", justifyContent:"center", alignItem:"center", padding:20}}>
            <h2>Validate TTS data</h2>
        </div>
        <LoadFileForm />
        <Loader promiseTracker={usePromiseTracker} type="ThreeDots" color="#2BAD60" height="100" width="100"/>

    </div>
  );
}

export default App;
