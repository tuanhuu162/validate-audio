import React from 'react';
import { Button } from "react-bootstrap";


export function SubmitButton(props) {
    return(
        <div style={{ display:"flex", justifyContent:"center", alignItem:"center" }}>
            <Button variant="primary" type="submit" >
                Save
            </Button>
        </div>
    ); 
};