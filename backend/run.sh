#!/bin/bash
# GUNICORN_CMD_ARGS="--bind=127.0.0.1 --workers=3"
gunicorn -w 1 -b 0.0.0.0:5000 -t 300 main:app \
--workers=3 \
--access-logfile=- \
--log-level debug \
--capture-output \
--error-logfile=-