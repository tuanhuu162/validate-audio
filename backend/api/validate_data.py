from flask import Blueprint, request, jsonify, Response, current_app
import scipy.io.wavfile as sio 
import os
import struct
import numpy as np
import shutil
from io import BytesIO
import zipfile
import pandas as pd
from urllib.parse import quote
import config
from utils.utils import allow_file, validate_data
from utils.status import *
import json



validate_data_api = Blueprint('validate_data', __name__)
cache = {}

@validate_data_api.route('/', methods=['POST'])
def upload():
    """
        API for upload and validate audio and transcription file
    """
    # audioFiles = request.files.getlist("file[]")
    # transcription = request.file['file']
    try:
        global cache
        transcription = request.files['transcription']
        audioFiles = request.files['audio']
        if not allow_file(audioFiles.filename, ["zip"]):
            raise Exception("Audio file must be zip file")
        if not allow_file(transcription.filename, ["csv"]):
            raise Exception("Transcription file must be csv")
        transcript = {}
        for line in transcription.read().decode("utf-8").split("\n"):
            if line != '':
                transcript[line.split("~")[0]]= line.split("~")[1] 
        if os.path.exists(config.upload_folder):
            shutil.rmtree(config.upload_folder)
        else:
            os.mkdir(config.upload_folder)
        audio_raw_name = ".".join(audioFiles.filename.split(".")[:-1])
        with zipfile.ZipFile(audioFiles) as audio:
            audio.extractall(config.upload_folder)
        os.mkdir(os.path.join(config.upload_folder, "wav"))
        for name in os.listdir(os.path.join(config.upload_folder, audio_raw_name)):
            samplerate, data = sio.read(os.path.join(config.upload_folder, audio_raw_name, name))
            current_app.logger.info("Length data: " + str(len(data)))
            framelength = len(data)
            if framelength % 2048 != 0:
                pos = ((int(framelength / 2048) + 1)* 2048) - framelength 
                value = np.array([0 for i in range(int(pos))], dtype=np.int16)
                data = np.concatenate((data, value), axis=0)
            current_app.logger.info("length data after validate: " + str(len(data)))
            audio_path = os.path.join(config.upload_folder, "wav",name)
            sio.write(audio_path, samplerate, data)
            status = FILE_UPLOAD_SUCCESS
            name_raw = name.split(".")[0]
            if name_raw in cache:
                status = FILE_DUPLICATED
            origin_text = transcript[name_raw]
            google_content, distance = validate_data(audio_path, origin_text)
            if status == FILE_UPLOAD_SUCCESS:
                status = TRANSCRIPT_MATCH_AUDIO if distance == 0 else TRANSCRIPT_DO_NOT_MATCH_AUDIO

            if(status == FILE_DUPLICATED):
                compare_status = "#fc03e3"
            elif (status == 5): 
                compare_status = "#eb3434"
            elif (status == 4):
                compare_status = "#20db0f"

            cache[name_raw] = {
                "name": name_raw,
                "audio": audio_path,
                "text": transcript[name_raw],
                "google": google_content,
                "distance": distance,
                "status": compare_status
            }
            current_app.logger.info("Modify file " + name_raw + " successfull")
        with open(os.path.join(config.upload_folder, "results.json"), "w", encoding="utf-8") as file:
            json.dump(cache, file)
        resp = jsonify({
            "data": [cache[i] for i in cache]
        })
        resp.status_code = 200
        return resp
    except Exception as e:
        current_app.logger.error(str(e))
        resp = jsonify({
            "message": str(e)
        })
        resp.status_code = 400
        return resp


@validate_data_api.route('/get', methods=['GET'])
def pick_audio():
    """
    API getting audio information
    param:
        name: audio file's name
        transcript: the correct transcript of audio file
    """
    try:
        global cache
        name = request.args.get("name") if "name" in request.args else None
        if len(cache) == 0 and os.path.exists(os.path.join(config.upload_folder, 'results.json')):
            with open(os.path.join(config.upload_folder, 'results.json'), "r") as file:
                cache = json.load(file, encoding="utf-8")
        if name not in cache or len(cache) == 0:
            raise Exception("File not found")
        transcript = request.args.get('transcript') if "transcript" in request.args else None
        if transcript is not None:
            if transcript == '':
                raise Exception("transcript cannot be empty")
            cache[name]['text'] = transcript
            cache[name]['status'] = FILE_UPDATE_SUCCESS
            resp = jsonify({
                "message": "update success"
            })
            return resp
        else:
            headers = {"Content-Disposition":
                        "attachment;"
                        "filename*=UTF-8''{utf_filename}".format(
                            utf_filename=quote(f"{name}.wav"))}
            mimetype = "audio/wav"
            return Response(
                    open(cache[name]["audio"], "rb").read(),
                    mimetype=mimetype,
                    headers=headers)
    except Exception as e:
        current_app.logger.error(str(e))
        resp = jsonify({
            "message": str(e)
        })
        resp.status_code = 400
        return resp