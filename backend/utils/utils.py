import speech_recognition as sr
import wave
import editdistance

def allow_file(filename, extension):
    return filename.split(".")[-1] in extension


def wav2text(path_wav_file="", language="en-EN"):
    '''
    Get content text of  wav file
    :param path_wav_file:
    :param language: en-En, vi-VI, ...
    :return: string or None if can't recognize
    '''
    audio_file = (path_wav_file)
    # use the audio file as the audio source
    r = sr.Recognizer()
    with sr.AudioFile(audio_file) as source:
        # reads the audio file. Here we use record instead of
        # listen
        audio = r.record(source)
    content = None
    try:
        content = r.recognize_google(audio, language="vi-VI", show_all=True)
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")

    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0} ".format(e))
    content = content['alternative'][0]['transcript']
    return content


def validate_data(audio, transcript, lang="en-EN"):
    audio_file = (audio)
    r = sr.Recognizer()
    with sr.AudioFile(audio_file) as source:
        # reads the audio file. Here we use record instead of
        # listen
        audio = r.record(source)
    content = None
    try:
        content = r.recognize_google(audio, language="vi-VI", show_all=True)
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")

    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0} ".format(e))
    content = content['alternative'][0]['transcript']
    distance = editdistance.eval(transcript.lower(), content.lower())
    return content, distance
    