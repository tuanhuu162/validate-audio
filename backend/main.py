from flask import Flask, request, render_template
from flask import jsonify
import logging
from logging.handlers import RotatingFileHandler
import config 

app = Flask(__name__)
#time for session life
#app.permanent_session_lifetime = timedelta(minutes=120)
app.secret_key = 'A43r721hfdavfAYTtDYTEv/,123'
app.config['UPLOAD_FOLDER'] = config.upload_folder

from flask_cors import CORS
from api.validate_data import validate_data_api

cors = CORS(app)

app.register_blueprint(validate_data_api)


if __name__ == '__main__':
    # Config logging to both show on console and write to file-----------------
    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()

    handler = RotatingFileHandler(config.log_file, maxBytes=10000, backupCount=1)
    handler.setFormatter(logFormatter)
    handler.setLevel(logging.INFO)
    rootLogger.addHandler(handler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)
    # ------------------------------------------------Config logging completed!

    app.run(host=config.HOST, port=int(config.PORT), debug=True, threaded=True)
